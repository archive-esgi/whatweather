import { app } from '../main.js';
import Header from './layout/Header.js';
import Footer from './layout/Footer.js';
import CityCard from './others/CityCard.js';

function generate() {
    // Add header to the virtual DOM
    Header.generate();

    // Add Hero to virtual DOM
    app.createElement('section', { class:'hero is-primary' },
        app.createElement('div', { class:'hero-body' },
            app.createElement('div', { class:'container' },
                app.createElement('h1', { class:'title' }, 'Africa'),
                app.createElement('h2', { class:'subtitle' }, '')
            )
        )
    );

    // Add content to virtual DOM
    app.createElement('div', { class:'container' },
        app.createElement('div', { class:'section' },
            app.createElement('div', { class:'row columns is-multiline' },
                CityCard.generate('Abuja,ng'),
                CityCard.generate('Accra,gh'),
                CityCard.generate('Algiers,dz'),
                CityCard.generate('Antananarivo,mg'),
                CityCard.generate('Bamako,ml'),
                CityCard.generate('Cairo,eg'),
                CityCard.generate('Cape Town,za'),
                CityCard.generate('Dakar,sn'),
                CityCard.generate('Douala,cm'),
                CityCard.generate('Rabat,ma'),
                CityCard.generate('Nouakchott,mr'),
                CityCard.generate('Tunis,tn'),
            )
        )
    );

    // Add footer to the virtual DOM
    Footer.generate();
}

export default {
    generate,
}
