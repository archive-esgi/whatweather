import { app } from '../main.js';
import Header from './layout/Header.js';
import SearchBar from './home/SearchBar.js';
import Footer from './layout/Footer.js';
import CityCard from './others/CityCard.js';
import LocalCityCard from './home/LocalCityCard.js';

function generate() {
    // Add header to the virtual DOM
    Header.generate();

    // Add search bar to the virtual DOM
    app.createElement('section', { class:'hero is-primary' },
        app.createElement('div', { class:'hero-body' },
            app.createElement('div', { class:'container' },
                app.createElement('div', { class:'section' },
                    SearchBar.generate()
                )
            )
        )
    );

    // Add content to virtual DOM
    app.createElement('div', { class:'container' },
        app.createElement('div', { class:'section' },
            app.createElement('div', { class:'row columns' },
                LocalCityCard.generate(true)
            ),
            app.createElement('div', { class:'row columns is-multiline' },
                CityCard.generate('London,gb'),
                CityCard.generate('Paris,fr'),
                CityCard.generate('Dublin,ie'),
                CityCard.generate('Roma,it'),
                CityCard.generate('Berlin,de'),
                CityCard.generate('Madrid,es')
            )
        )
    );

    // Add footer to the virtual DOM
    Footer.generate();
}

export default {
    generate,
}
