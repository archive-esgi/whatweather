import {app} from '../main.js';
import Header from './layout/Header.js';
import Footer from './layout/Footer.js';
import CityCard from './others/CityCard.js';

function generate() {
    // Add header to the virtual DOM
    Header.generate();

    // Add Hero to virtual DOM
    app.createElement('section', {class: 'hero is-primary'},
        app.createElement('div', {class: 'hero-body'},
            app.createElement('div', {class: 'container'},
                app.createElement('h1', {class: 'title'}, 'Europe'),
                app.createElement('h2', {class: 'subtitle'}, '')
            )
        )
    );

    // Add content to virtual DOM
    app.createElement('div', {class: 'container'},
        app.createElement('div', {class: 'section'},
            app.createElement('div', {class: 'row columns is-multiline'},
                CityCard.generate('London,gb'),
                CityCard.generate('Paris,fr'),
                CityCard.generate('Dublin,ie'),
                CityCard.generate('Roma,it'),
                CityCard.generate('Berlin,de'),
                CityCard.generate('Madrid,es'),
                CityCard.generate('Moscow,ru'),
                CityCard.generate('Lisbon,pt'),
                CityCard.generate('Bern,ch'),
                CityCard.generate('Prague,cz'),
                CityCard.generate('Helsinki,fi'),
                CityCard.generate('Copenhagen,dk'),
                CityCard.generate('Vienna,at'),
                CityCard.generate('Brussels,be'),
                CityCard.generate('Oslo,no'),
                CityCard.generate('Budapest,hu'),
                CityCard.generate('Luxembourg,lu'),
                CityCard.generate('Amsterdam,nl'),
            )
        )
    );

    // Add footer to the virtual DOM
    Footer.generate();
}

export default {
    generate,
}
