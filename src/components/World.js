import { app } from '../main.js';
import Header from './layout/Header.js';
import Footer from './layout/Footer.js';
import CityCard from './others/CityCard.js';

function generate() {
    // Add header to the virtual DOM
    Header.generate();

    // Add Hero to virtual DOM
    app.createElement('section', { class:'hero is-primary' },
        app.createElement('div', { class:'hero-body' },
            app.createElement('div', { class:'container' },
                app.createElement('h1', { class:'title' }, 'World'),
                app.createElement('h2', { class:'subtitle' }, '')
            )
        )
    );

    app.createElement('div', { class:'container' },
        app.createElement('div', { class:'section' },
            app.createElement('div', { class:'row columns is-multiline' },
                CityCard.generate('London,gb'),
                CityCard.generate('Paris,fr'),
                CityCard.generate('Madrid,es'),
                CityCard.generate('Moscow,ru'),
                CityCard.generate('Lisbon,pt'),
                CityCard.generate('Bern,ch'),
                CityCard.generate('Canberra,au'),
                CityCard.generate('Abuja,ng'),
                CityCard.generate('Accra,gh'),
                CityCard.generate('Algiers,dz'),
                CityCard.generate('Antananarivo,mg'),
                CityCard.generate('Cape Town,za'),
                CityCard.generate('New York,us'),
                CityCard.generate('Los Angeles,us'),
                CityCard.generate('Bogota,co'),
                CityCard.generate('Brasilia,br'),
                CityCard.generate('Buenos Aires,ar'),
                CityCard.generate('Caracas,ve'),
                CityCard.generate('Guatemala City,gt'),
                CityCard.generate('La Havane,cu'),
                CityCard.generate('Pékin,cn'),
                CityCard.generate('Achgabat,tm'),
                CityCard.generate('Amman,jo'),
                CityCard.generate('Ankara,tr'),
                CityCard.generate('Suva,fj'),
                CityCard.generate('Port Moresby,pg'),
                CityCard.generate('Nuku’alofa,to'),
                CityCard.generate('Funafuti,tv'),
                CityCard.generate('Port-Vila, vu')
            )
        )
    );

    // Add footer to the virtual DOM
    Footer.generate();
}

export default {
    generate,
}
