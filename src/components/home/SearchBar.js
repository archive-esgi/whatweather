import { app } from '../../main.js';

function generate() {
    return app.createElement('div', { class:'box' },
        app.createElement('form', { method:'GET', action:app.router.getPathByRouteName('city') },
            app.createElement('div', { class:'field has-addons' },
                app.createElement('div', { class:'control is-expanded' },
                    app.createElement('input', { class:'input has-text-centered', name:'city', type:'search', placeholder:"Type a city name (example: London,gb)" })
                ),
                app.createElement('div', { class:'control' },
                    app.createElement('input', { class:'button is-info', type:'submit', value:'Search' })
                )
            )
        )
    );
}

export default {
    generate,
};
