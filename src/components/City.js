import { app } from '../main.js';
import Header from './layout/Header.js';
import Footer from './layout/Footer.js';
import CityCard from './others/CityCard.js';
import SearchBar from './home/SearchBar.js';

function getParam() {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const city = urlParams.get('city');

    return city;
}

function generate() {
    // Add header to the virtual DOM
    Header.generate();

    // Add Hero to virtual DOM
    app.createElement('section', { class:'hero is-primary' },
        app.createElement('div', { class:'hero-body' },
            app.createElement('div', { class:'container' },
                app.createElement('h1', { class:'title' }, 'City'),
                SearchBar.generate()
            )
        )
    );

    app.createElement('div', { class:'container' },
        app.createElement('div', { class:'section' },
            app.createElement('div', { class:'row columns is-multiline' },
                CityCard.generate(getParam(), true),
            )
        )
    );

    // Add footer to the virtual DOM
    Footer.generate();
}

export default {
    generate,
}
